<div class="container">
    <nav class="navbar navbar-default navbar-fixed-bottom" style="width: 50%; margin: 0 auto;">
        <div class="container-fluid">
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <a class="navbar-text">
                        <?php echo __('Cambiar Idioma') ?>:
                    </a>
                    <li>
                        <?php echo $this->Html->image('spain-flag.png', [
                                'height' => '24',
                                'alt' => 'Español',
                                'url'=>['controller' => 'App','action' => 'changeLanguage', 'es_MX']]);
                        ?>
                    </li>
                    <li>
                        <?php echo $this->Html->image('united-kingdom-flag.png', [
                                'height' => '24',
                                'alt' => 'Inglés',
                                'url'=>['controller' => 'App','action' => 'changeLanguage', 'en_US']]);
                        ?>
                    </li>
                    <li>
                        <?php echo $this->Html->image('france-flag.png', [
                                'height' => '24',
                                'alt' => 'Francés',
                                'url'=>['controller' => 'App','action' => 'changeLanguage', 'fr_FR']]);
                        ?>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</div>