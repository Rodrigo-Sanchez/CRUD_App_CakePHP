<div class="container">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li>
                        <?php echo $this->Html->image('home.png', [
                                'alt' => 'Página de inicio',
                                'url'=>['controller' => 'Usuarios', 'action' => 'index']]);
                        ?>
                    </li>
                    <li>
                        <a class="navbar-brand"><?php echo __('Sistema CRUD') ?></a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <?php echo __('Acciones') ?>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <?php echo $this->Html->link(__('Cambiar Contraseña'), ['controller' => 'Usuarios', 'action' => 'change_password']) ?>
                            </li>
                            <li>
                                <?php echo $this->Html->link(__('Crear Usuario'), ['controller' => 'Usuarios', 'action' => 'add']) ?>
                            </li>
                            <li>
                                <?php echo $this->Html->link(__('Cerrar Sesión'), ['controller' => 'Usuarios', 'action' => 'logout']) ?>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</div>