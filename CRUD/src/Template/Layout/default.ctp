<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Sistema CRUD';
?>
<!DOCTYPE html>
<html>
<head>
    <?php echo $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?php echo $cakeDescription ?>:
        <?php echo $this->fetch('title') ?>
    </title>
    <?php echo $this->Html->meta('icon') ?>

    <?php echo $this->Html->css(['bootstrap.css', 'https://fonts.googleapis.com/css?family=Roboto|Varela+Round']) ?>
    <?php echo $this->Html->script(['jquery-3.3.1.js', 'bootstrap.js']) ?>

    <?php echo $this->fetch('meta') ?>
    <?php echo $this->fetch('css') ?>
    <?php echo $this->fetch('script') ?>
    
    <script src='https://www.google.com/recaptcha/api.js?render=6LeZxooUAAAAAMGLEfu5BXiAA_7nzJ_t6p9DJDWX'></script>
</head>
<body>
    <?php echo $this->element('usuarios/menu') ?>

    <?php echo $this->Flash->render() ?>
    <div class="container">
        <?php echo $this->fetch('content') ?>
    </div>

    <div class="g-recaptcha" data-theme="dark"></div>

    <?php echo $this->element('usuarios/footer') ?>

</body>
</html>
