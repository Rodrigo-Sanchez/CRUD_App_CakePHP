<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Usuario[]|\Cake\Collection\CollectionInterface $usuarios
 */
?>
<div class="usuarios index large-9 medium-8 columns content">
    <h3><?php echo __('Usuarios') ?></h3>
    <table class="table table-striped table-hover table-bordered" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?php echo $this->Paginator->sort('apellidopaterno', ['label' => __('Primer Apellido')]) ?></th>
                <th scope="col"><?php echo $this->Paginator->sort('apellidomaterno', ['label' => __('Segundo Apellido')]) ?></th>
                <th scope="col"><?php echo $this->Paginator->sort('nombre', ['label' => __('Nombre(s)')]) ?></th>
                <th scope="col"><?php echo $this->Paginator->sort('email', ['label' => __('Dirección de correo electrónico')]) ?></th>
                <th scope="col"><?php echo __('Es administrador') ?></th>
                <th scope="col"><?php echo __('Está habilitado') ?></th>
                <?php if(isset($esAdministrador)) : ?>
                    <th scope="col" class="actions"><?php echo __('Acciones') ?></th>
                <?php endif; ?>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($usuarios as $usuario): ?>
                <tr>
                    <td><?php echo h($usuario->apellidopaterno) ?></td>
                    <td><?php echo h($usuario->apellidomaterno) ?></td>
                    <td><?php echo h($usuario->nombre) ?></td>
                    <td><?php echo h($usuario->email) ?></td>
                    <td><?php echo h($usuario->rol) ? $this->Html->image('check.png', ['alt' => 'Es administrador']) : $this->Html->image('error.png', ['alt' => 'No es administrador']) ?></td>
                    <td><?php echo h($usuario->estatus) ? $this->Html->image('check.png', ['alt' => 'Está activado']) : $this->Html->image('error.png', ['alt' => 'No está activado']) ?></td>
                    <?php if(isset($esAdministrador)) : ?>
                        <td class="actions">
                            <?php echo $this->Html->link(__('Ver'), ['action' => 'view', $usuario->id], ['class'=>'btn btn-primary btn-sm']) ?>
                            <?php echo $this->Html->link(__('Editar'), ['action' => 'edit', $usuario->id], ['class'=>'btn btn-warning btn-sm']) ?>
                            <?php echo $this->Form->postLink(__('Borrar'), ['action' => 'delete', $usuario->id], ['confirm' => __('¿Estás seguro que quieres borrar el usuario {0} con ID {1}?', $usuario->nombre, $usuario->id), 'class'=>'btn btn-danger btn-sm']) ?>
                        </td>
                    <?php endif; ?>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="paginator">
                <ul class="pagination">
                    <?php echo $this->Paginator->first(__('<< Primero')) ?>
                    <?php echo $this->Paginator->prev(__('< Anterior')) ?>
                    <?php echo $this->Paginator->numbers(['before' => '', 'after' => '']) ?>
                    <?php echo $this->Paginator->next(__('Siguiente >')) ?>
                    <?php echo $this->Paginator->last(__('Último >>')) ?>
                </ul>
                <p><?php echo $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, mostrando {{current}} registros de {{count}} totales')]) ?></p>
            </div>
        </div>
    </div>
</div>
