<h1><?php echo __('Cambiar Contraseña') ?></h1>
<?php 
    echo $this->Form->create($usuario, ['type' => 'file']);
    echo $this->Html->image('../files/usuarios/photo/'.$photo_dir.'/'.$photo, 
                        ['alt' => $usuario->nombre, 'class' => 'img-responsive img-thumbnail center-block']);
    echo $this->Form->control('actualPassword', ['label' => __('Contraseña Actual'), 'type' => 'password']);
    echo $this->Form->control('newPassword', ['label' => __('Nueva Contraseña'), 'type' => 'password']);
    echo $this->Form->control('confirmPassword', ['label' => __('Confirmar Nueva Contraseña'), 'type' => 'password']);
    echo $this->Form->button(__('Cambiar'));
    echo $this->Form->end(); ?>