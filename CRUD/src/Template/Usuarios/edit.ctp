<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Usuario $usuario
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="nav nav-pills">
        <li class="navbar-brand">
            <?php echo __('Acciones').':' ?>
        </li>
        <li><?php echo $this->Form->postLink(
                __('Borrar Usuario'),
                ['action' => 'delete', $usuario->id],
                ['confirm' => __('¿Estás seguro que quieres borrar el usuario, {0} con id, {1}?', $usuario->nombre, $usuario->id)])
            ?>
        </li>
    </ul>
</nav>
<div class="usuarios form large-9 medium-8 columns content">
    <?php echo $this->Form->create($usuario, ['type' => 'file']) ?>
    <fieldset>
        <legend><?php echo __('Editar Usuario') ?></legend>
        <?php
            echo $this->Html->image('../files/usuarios/photo/'.$usuario->photo_dir.'/'.$usuario->photo, 
                                    ['alt' => $usuario->nombre, 'class' => 'img-responsive img-thumbnail center-block']);
            echo $this->Form->control('apellidopaterno', ['label' => (__('Primer Apellido').'*')]);
            echo $this->Form->control('apellidomaterno', ['label' => __('Segundo Apellido')]);
            echo $this->Form->control('nombre', ['label' => __('Nombre(s)')]);
            echo $this->Form->control('password', ['label' => (__('Contraseña').'*')]);
            echo $this->Form->input('photo', ['type' => 'file', 'label' => (__('Subir Foto del Perfil').'*')]);
            echo $this->Form->control('estatus', ['label' => __('Está Habilitado')]);
            echo $this->Form->control('rol', ['label' => __('Es Administrador')]);
        ?>
    </fieldset>
    <?php echo $this->Form->button(__('Guardar')) ?>
    <?php echo $this->Form->end() ?>
    <?php echo '<b>*: ',__('Este Campo es Obligatorio'),'.</b>' ?>
</div>
