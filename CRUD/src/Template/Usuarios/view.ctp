<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Usuario $usuario
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="nav nav-pills">
        <li class="navbar-brand"><?php echo __('Acciones:') ?></li>
        <li><?php echo $this->Form->postLink(
                __('Borrar Usuario'),
                ['action' => 'delete', $usuario->id],
                ['confirm' => __('¿Estás seguro que quieres borrar el usuario, {0} con id: {1}.', $usuario->nombre, $usuario->id)])
            ?>
        </li>
        <li><?php echo $this->Html->link(
                __('Editar Usuario'), 
                ['action' => 'edit', $usuario->id]) 
            ?> 
        </li>
    </ul>
</nav>
<div class="usuarios view large-9 medium-8 columns content">
    <h3><?php echo h($usuario->nombre), " ", h($usuario->apellidopaterno), " ", h($usuario->apellidomaterno) ?></h3>
    <?= $this->Html->link(__('Export to PDF'), ['action' => 'view', $usuario->id, '_ext' => 'pdf']); ?>
    <table class="table table-striped table-hover table-bordered vertical-table" cellpadding="0" cellspacing="0">
        <tr>
            <th scope="row"><?php echo __('Dirección de correo electrónico:') ?></th>
            <td><?php echo h($usuario->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?php echo __('Contraseña:') ?></th>
            <td><?php echo h($usuario->password) ?></td>
        </tr>
        <tr>
            <th scope="row"><?php echo __('Nombre(s):') ?></th>
            <td><?php echo h($usuario->nombre), '.' ?></td>
        </tr>
        <tr>
            <th scope="row"><?php echo __('Primer Apellido:') ?></th>
            <td><?php echo h($usuario->apellidopaterno), '.' ?></td>
        </tr>
        <tr>
            <th scope="row"><?php echo __('Segundo Apellido:') ?></th>
            <td><?php echo h($usuario->apellidomaterno), '.' ?></td>
        </tr>
        <tr>
            <th scope="row"><?php echo __('Id:') ?></th>
            <td><?php echo $this->Number->format($usuario->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?php echo __('Estatus:') ?></th>
            <td><?php echo $usuario->estatus ? 'Está habilitado.' : 'No está habilitado.'; ?></td>
        </tr>
        <tr>
            <th scope="row"><?php echo __('Rol:') ?></th>
            <td><?php echo $usuario->rol ? 'Es administrador.' : 'No es administrador.'; ?></td>
        </tr>
    </table>
</div>
