<?php
    define('SITE_KEY', '6LeZxooUAAAAAMGLEfu5BXiAA_7nzJ_t6p9DJDWX');
    define('SECRET_KEY', '6LeZxooUAAAAAA7rk7MVwFj4MzvqwhhQpHJwBmDj');

if($_POST) {
    function getCaptcha($SecretKey) {
        $Response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".SECRET_KEY."&response={$SecretKey}");
        $Return = json_decode($Response);
        return $Return;
    }
    $Return = getCaptcha($_POST['g-recaptcha-response']);

    // Nos muestra las características de la respuesta del reCAPTCHA.
    // var_dump($Return);

    if($Return->success == true && $Return->score > 0.5) {
        echo "Bienvenido, humano. :3";
    } else {
        echo "Eres un boot. :c";
    }
}

?>

<h1><?php echo __('Inicio de sesión') ?></h1>
<?php echo $this->Form->create() ?>
<?php echo $this->Form->control('email', ['label' => __('Dirección de correo electrónico')]) ?>
<?php echo $this->Form->control('password', ['label' => __('Contraseña')]) ?>
<?php echo $this->Form->input('Respuesta', ['id' => 'g-recaptcha-response', 'name' => 'g-recaptcha-response', 'type' => 'hidden']) ?>
<?php echo $this->Form->button(__('Iniciar')) ?>
<br>
<?php echo $this->Html->link(__('Cambiar Contraseña'), ['controller' => 'Usuarios', 'action' => 'reset_password']) ?>
<?php echo $this->Form->end() ?>

<script>
    grecaptcha.ready(function() {
        grecaptcha.execute('<?php echo SITE_KEY; ?>', {action: 'login'})
        .then(function(token) {
            // Verify the token on the server.
            document.getElementById('g-recaptcha-response').value=token;
        });
    });
</script>