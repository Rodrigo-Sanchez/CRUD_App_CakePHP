<h1><?php echo __('Reestablecer contraseña') ?></h1>
<?php echo $this->Form->create() ?>
<?php echo $this->Form->control('email', ['label' => __('Dirección de correo electrónico')]) ?>
<?php echo $this->Form->button(__('Reestablecer')) ?>
<?php echo $this->Form->end() ?>