<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Usuario $usuario
 */
?>

<div class="usuarios form large-9 medium-8 columns content">
    <?php echo $this->Form->create($usuario, ['type' => 'file']) ?>
    <fieldset>
        <legend><?php echo __('Crear Usuario') ?></legend>
        <?php
            echo $this->Form->control('email', ['type' => 'email', 'label' => __('Dirección de correo electrónico').'*']);
            echo $this->Form->input('password', ['type' => 'text', 'label' => __('Contraseña').'*', 'value' => $this->Usuario->generateRandomString()]);
            echo $this->Form->control('apellidopaterno', ['label' => __('Primer Apellido').'*']);
            echo $this->Form->control('apellidomaterno', ['label' => __('Segundo Apellido')]);
            echo $this->Form->control('nombre', ['label' => __('Nombre(s)').'*']);
            echo $this->Form->input('photo', ['type' => 'file', 'label' => __('Subir Foto del Perfil')]);
            echo $this->Form->control('estatus', ['label' => __('Está Habilitado')]);
            echo $this->Form->control('rol', ['label' => __('Es Admnistrador')]);
        ?>
    </fieldset>
    <?php echo $this->Form->button(__('Enviar')) ?>
    <?php echo $this->Form->end() ?>
    <?php echo '<b>*: ',__('Este Campo es Obligatorio'),'.</b>' ?>
</div>