<?php
namespace App\Mailer;

use Cake\Mailer\Mailer;
use Cake\FileSystem\File;

/**
 * Usuarios mailer.
 */
class UsuariosMailer extends Mailer
{

    /**
     * Mailer's name.
     *
     * @var string
     */
    public static $name = 'Usuarios';

    public function sendMail($usuario, $password) 
    {
        $this->to($usuario->email)
             ->profile('gmail')
             ->emailFormat('html')
             ->template('email_template')
             ->layout('usuarios')
             ->setViewVars(['nombre' => h($usuario->nombre), 'password' => h($password)])
             ->subject(sprintf('¡Bienvenido, %s! :)', $usuario->nombre));
    }

    public function resetPassword($usuario, $password) 
    {
        $this->to($usuario->email)
             ->profile('gmail')
             ->emailFormat('html')
             ->template('reset_password_template')
             ->layout('usuarios')
             ->setViewVars(['nombre' => h($usuario->nombre), 'password' => h($password)])
             ->subject(sprintf('%s, esta es tu nueva contraseña. :)', $usuario->nombre));
    }
}
