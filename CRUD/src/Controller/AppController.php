<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\I18n\I18n;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');

        $this->loadComponent('Auth', [
            'authorize'=> 'Controller',
            'authenticate' => [
                'Form' => [
                    'userModel' => 'Usuarios',
                    'fields' => [
                        'username' => 'email',
                        'password' => 'password'
                    ]
                ]
            ],
            'loginAction' => [
                'controller' => 'Usuarios',
                'action' => 'login'
            ],
             // use isAuthorized in Controllers
             'authorize' => ['Controller'],
             'authError' => 'Inicia sesión para acceder.',
             // If unauthorized, return them to page they were just on
            'unauthorizedRedirect' => $this->referer()
        ]);

        /*
         * Enable the following component for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
    }

    public function changeLanguage($language=null)
    {
        if($language!=null && in_array($language, ['en_US', 'es_MX', 'fr_FR']))
        {
            $this->request->session()->write('Config.language', $language);
            return $this->redirect($this->referer());
        }
        else
        {
            $this->request->session()->write('Config.language', I18n::locale());
            return $this->redirect($this->referer());
        }
    }

    public function beforeFilter(Event $event) 
    {
        // Permite mostrar las acciones de nuestros controladores
        // También habilita las acciones de sólo lectura.
        $this->Auth->allow(['login', 'logout', 'changeLanguage', 'resetPassword']);

        if($this->request->getSession()->check('Config.language'))
        {
            I18n::setLocale($this->request->getSession()->read('Config.language'));
        }
        else
        {
            $this->request->getSession()->write('Config.language', I18n::locale());
        }
    }

    /**
     * Método isAuthored
     *
     * @param string|null $user Usuario user.
     */
    public function isAuthorized($user) {
        return true;
    }
}
