<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Mailer\MailerAwareTrait;
use Cake\ORM\TableRegistry; // Para hacer los insert.

/**
 * Usuarios Controller
 *
 * @property \App\Model\Table\UsuariosTable $Usuarios
 *
 * @method \App\Model\Entity\Usuario[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsuariosController extends AppController
{

    // Variable para indicar los valores de la paginación.
    public $paginate = [
        'limit' => 10,
        'order' => [
            'Usuarios.apellidopaterno' => 'asc'
        ]
    ];

    /**
     * Método index
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        // Verificamos que el usuario sea administrador.
        if ($this->getRequest()->getSession()->read('Auth.User.rol')) {
            $this->set('esAdministrador', true);
        }

        // Hacemos la paginación de los Usuarios.
        $usuarios = $this->paginate($this->Usuarios);
        
        // Definimos la variable usuarios para la vista con la variable $usuarios definida anteriormente.
        $this->set(compact('usuarios'));
    }

    /**
     * Método view
     *
     * @param string|null $id Usuario id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $usuario = $this->Usuarios->get($id, [
            'contain' => []
        ]);

        // Con la variable $usuario definida en este método creamos la variable $usuario para la vista.
        $this->set(compact('usuario'));
    }

    /**
     * Método add
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    use MailerAwareTrait;
    public function add()
    {
        // Creamos una nueva entidad de acuerdo al modelo Usuarios.
        $usuario = $this->Usuarios->newEntity();

        if ($this->request->is('post')) {
            $usuario = $this->Usuarios->patchEntity($usuario, $this->request->getData());

            // Guardamos el nuevo usuario en la base de datos.
            if ($this->Usuarios->save($usuario)) {
                $this->Flash->success(__('El usuario ha sido guardado correctamente.'));
                
                // Guardamos la contraseña sin hashear para enviarla por correo.
                $password = $this->request->getData('password');

                // Enviamos el correo de bienvenida con la contraseña generada automáticamente.
                // Obtenemos el mailer de usuarios y mandamos a llamar a la función 
                // send_mail con $usuario y $password como parámetros.
                $this->getMailer('Usuarios')->send('sendMail', [$usuario, $password]);

                // Hacemos el insert correspondiente en la bitácora de la base de datos.
                $registrosTable = TableRegistry::get('Registros');
                $registro = $registrosTable->newEntity();
                $registro->usuarioid = $this->getRequest()->getSession()->read('Auth.User.id');
                $registro->email = $this->getRequest()->getSession()->read('Auth.User.email');
                $registro->accion = '1';
                $registro->fechacreacion = 'NOW()';

                // Guardamos el nuevo registro de la bitácora en la base de datos.
                $registrosTable->save($registro);

                // Redirigimos al index.
                return $this->redirect(['action' => 'index']);
            }

            $this->Flash->error(__('El usuario no pudo ser guardado. Por favor inténtalo de nuevo.'));
        }
        
        // Con la variable $usuario definida en este método creamos la variable $usuario para la vista.
        $this->set(compact('usuario'));
    }

    /**
     * Método edit
     *
     * @param string|null $id Usuario id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $usuario = $this->Usuarios->get($id, [
            'contain' => []
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $usuario = $this->Usuarios->patchEntity($usuario, $this->request->getData());

            // Si se guarda el usuario, procedemos.
            if ($this->Usuarios->save($usuario)) {

                // Hacemos el insert correspondiente en la bitácora de la base de datos.
                $registrosTable = TableRegistry::get('Registros');
                $registro = $registrosTable->newEntity();
                $registro->usuarioid = $this->getRequest()->getSession()->read('Auth.User.id');
                $registro->email = $this->getRequest()->getSession()->read('Auth.User.email');
                $registro->accion = '2';
                $registro->fechacreacion = 'NOW()';
                
                // Guardamos el nuevo registro de la bitácora en la base de datos.
                $registrosTable->save($registro);

                // Mostramos un mensaje de confirmación el usuario pudo ser guardado.
                $this->Flash->success(__('El usuario ha sido guardado correctamente.'));

                // Redirigimos al index.
                return $this->redirect(['action' => 'index']);
            }

            // Mostramos un mensaje de error que el usuario no pudo ser guardado.
            $this->Flash->error(__('El usuario no pudo ser guardado. Por favor inténtalo de nuevo.'));
        }

        // Con la variable $usuario definida en este método creamos la variable $usuario para la vista.
        $this->set(compact('usuario'));
    }

    /**
     * Método delete
     *
     * @param string|null $id Usuario id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        // Obtenemos el usuario registrado en la base de datos con el id especificado.
        $usuario = $this->Usuarios->get($id);
        
        // Si borramos el usuario entonces entramos en el ciclo.
        if ($this->Usuarios->delete($usuario)) {
                    
            // Hacemos el insert correspondiente en la bitácora de la base de datos.
            $registrosTable = TableRegistry::get('Registros');
            $registro = $registrosTable->newEntity();
            $registro->usuarioid = $this->getRequest()->getSession()->read('Auth.User.id');
            $registro->email = $this->getRequest()->getSession()->read('Auth.User.email');
            $registro->accion = '3';
            $registro->fechacreacion = 'NOW()';
            
            // Guardamos el nuevo registro de la bitácora en la base de datos.
            $registrosTable->save($registro);

            // Mostramos un mensaje que el usuario ha sido borrado exitosamente.
            $this->Flash->success(__('El usuario ha sido borrado correctamente.'));
        } else {

            // Mostramos un mensaje de error que el usuario no ha sido borrado.
            $this->Flash->error(__('El usuario no pudo ser borrado. Por favor inténtalo de nuevo.'));
        }

        // Redirigimos al index.
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Método login
     *
     */
    public function login()
    {
        if ($this->request->is('post')) {
            $usuario = $this->Auth->identify();

            // Verificamos si hay un usuario.
            if ($usuario) {
                $this->Auth->setUser($usuario);

                // Verificamos si está habilitado el usuario.
                if ($this->getRequest()->getSession()->read('Auth.User.estatus')) {

                    // Hacemos el insert correspondiente en la bitácora de la base de datos.
                    $registrosTable = TableRegistry::get('Registros');
                    $registro = $registrosTable->newEntity();
                    $registro->usuarioid = $this->getRequest()->getSession()->read('Auth.User.id');
                    $registro->email = $this->getRequest()->getSession()->read('Auth.User.email');
                    $registro->accion = '4';
                    $registro->fechacreacion = 'NOW()';

                    // Guardamos el nuevo registro de la bitácora en la base de datos.
                    $registrosTable->save($registro);

                    return $this->redirect(['controller' => 'usuarios', 'action' => 'index']);
                } else {

                    // Mostramos un mensaje de confirmación que no está activa la cuenta.
                    $this->Flash->error(__('Contacta al administrador, tu cuenta está deshabilitada.'));
                }
            } else {

                // Mostramos un mensaje de error que no se encontró ningún registro para el login.
                $this->Flash->error(__('Tu correo o contraseña son incorrectos.'));
            }
        }
    }

    /**
     * Método logout
     *
     */
    public function logout()
    {
        // Hacemos el insert correspondiente en la bitácora de la base de datos.
        $registrosTable = TableRegistry::get('Registros');
        $registro = $registrosTable->newEntity();
        $registro->usuarioid = $this->getRequest()->getSession()->read('Auth.User.id');
        $registro->email = $this->getRequest()->getSession()->read('Auth.User.email');
        $registro->accion = '5';
        $registro->fechacreacion = 'NOW()';

        // Guardamos el nuevo registro de la bitácora en la base de datos.
        $registrosTable->save($registro);
        
        // Mostramos un mensaje de confirmación que se cerró la sesión.
        $this->Flash->success(__('Acabas de cerrar sesión.'));

        // Redirigimos al index.
        return $this->redirect($this->Auth->logout());
    }

    /**
     * Método resetPassword
     *
     */
    public function resetPassword()
    {
        // Creamos una nueva entidad usuario de acuerdo al modelo Usuarios.
        $usuario = $this->Usuarios->newEntity();

        // Verificamos si la petición fue hecha por POST.
        if ($this->request->is('post')) {
            $usuario = $this->Usuarios->patchEntity($usuario, $this->request->getData());

            // Obtenemos el correo electrónico que se envía por POST.
            $email = $this->request->getData('email');

            // Obtenemos el registro de acuerdo al email.
            $query = $this->Usuarios->find()->where(['email LIKE' => $email]);

            // Este es un objeto Query, para obtener los resultados es necesario ejecutar o iterar.
            foreach ($query as $user) {
                $id = $user->id;
            }

            // Nos aseguramos que haya un registro con ese id.
            if(isset($id)) {

                // Obtenemos el usuario que tiene el usuario id.
                $usuario = $this->Usuarios->get($id);

                // Creamos una nueva contraseña generada aleatoriamente.
                $password = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(20/strlen($x)) )),1,20);

                // Hacemos el update en la base de datos con la nueva contraseña generada anteriormente.
                $usuariosTable = TableRegistry::get('Usuarios');

                // Modificamos el registro password de la base de datos con la nuevacontraseña.
                $usuario->password = $password;
                
                // Guardamos el usuario con la contraseña actualizada.
                $usuariosTable->save($usuario);

                // Enviamos el correo de restablecimiento con la contraseña generada automáticamente.
                // Obtenemos el mailer de usuarios y mandamos a llamar a la función 
                // reset_password con $usuario y $password como parámetros.
                $this->getMailer('Usuarios')->send('resetPassword', [$usuario, $password]);

                // Hacemos el insert correspondiente en la bitácora de la base de datos.
                $registrosTable = TableRegistry::get('Registros');
                $registro = $registrosTable->newEntity();
                $registro->usuarioid = $id;
                $registro->email = $email;
                $registro->accion = '6';
                $registro->fechacreacion = 'NOW()';

                // Guardamos el nuevo registro de la bitácora en la base de datos.
                $registrosTable->save($registro);

                // Mostramos un mensaje de confirmación que se realizó la transacción correctamente.
                $this->Flash->success(__('La nueva contraseña ha sido enviada por correo'),'.');

                // Redirigimos al login.
                return $this->redirect(['action' => 'login']);
            } else {
                
                // Mostramos un mensaje de confirmación que no se encontró ningún registro en la base de datos.
                $this->Flash->error(__('Asegúrate de introducir un correo registrado en el sistema.'),'.');
            }
        }
    }

    /**
     * Método changePassword
     *
     * @param string|null $id Usuario id.
     */
    public function changePassword()
    {

        // Creamos una nueva entidad de acuerdo al modelo Usuarios.
        $usuario = $this->Usuarios->newEntity();

        // Obtenemos el nombre de la foto del perfil de la sesión activa.
        $photo = $this->getRequest()->getSession()->read('Auth.User.photo');

        // Obtenemos el nombre de la ruta de la foto del perfil de la sesión activa.
        $photo_dir = $this->getRequest()->getSession()->read('Auth.User.photo_dir');

        // Con la variables de los argumentos de la función compact
        // definimos las variable con ese mismo nombre para la vista.
        $this->set(compact('photo', 'photo_dir'));

        // Checamos que la petición por la que mandamos la información es POST.
        if ($this->request->is('post')) {

            // Creamos un usuario con la información recibida por POST y usamos el esquema de validación especificado.
            $usuario = $this->Usuarios->patchEntity($usuario, $this->request->getData(), ['validate' => 'passwords']);

            // Obtenemos el correo electrónico de la sesión activa.
            $email = $this->getRequest()->getSession()->read('Auth.User.email');

            // Obtenemos el registro de acuerdo al email.
            $query = $this->Usuarios->find()->where(['email LIKE' => $email]);

            // Este es un objeto Query, para obtener los resultados es necesario ejecutar o iterar.
            foreach ($query as $user) {
                $id = $user->id;
            }

            // Obtenemos los datos del formulario que tiene como no
            $actualPassword = $this->request->data['actualPassword'];

            // Si hay un error, muestra un mensaje en caso contrario sigue con el flujo del programa.
            if($usuario->errors()) {

                // debug($usuario->errors()); // <- Muestra los errores de validación.
                $this->Flash->error(__('Ocurrió un error en el formulario, verifica los campos.'));
            } else if(password_verify($actualPassword, $usuario->password)) {
                
                // 
                $this->Flash->success(__('La contraseña actual no coincide con la de la base de datos.'));
            } else {

                // Nos aseguramos que haya un registro con ese id.
                if(isset($id)) {

                    // Obtenemos el usuario que tiene el usuario id.
                    $usuario = $this->Usuarios->get($id);

                    // Modificamos el valor de la contraseña por el valor 'newPassword' enviado por POST.
                    $usuario->password = $this->request->data['newPassword'];

                    // Guardamos el usuario.
                    if ($this->Usuarios->save($usuario)) {

                        // Hacemos el insert correspondiente en la bitácora de la base de datos.
                        $registrosTable = TableRegistry::get('Registros');
                        $registro = $registrosTable->newEntity();
                        $registro->usuarioid = $this->getRequest()->getSession()->read('Auth.User.id');
                        $registro->email = $this->getRequest()->getSession()->read('Auth.User.email');
                        $registro->accion = '7';
                        $registro->fechacreacion = 'NOW()';

                        // Guardamos el nuevo registro de la bitácora en la base de datos.
                        $registrosTable->save($registro);

                        // Mostramos un mensaje de confirmación que se ha guardado la nueva contraseña en la base de datos.
                        $this->Flash->success(__('Tu nueva contraseña ha sido guardada exitosamente.'));
                    } else {

                        // Mostramos un mensaje de confirmación que no se ha guardado la nueva contraseña en la base de datos.
                        $this->Flash->error(__('No se ha podido guardar tu nueva contraseña, inténtalo de nuevo.'));
                    }

                    // Redirigimos al index.
                    return $this->redirect(['action' => 'index']);
                } else {

                    // Mostramos un mensaje de confirmación que no se encontró ningún registro en la base de datos.
                    $this->Flash->error(__('Asegúrate de introducir un correo registrado en el sistema.'),'.');
                }
            }
        }

        // Con la variables de los argumentos de la función compact
        // definimos las variable con ese mismo nombre para la vista.
        $this->set(compact('usuario'));
    }

    /**
     * Método isAuthored
     *
     * @param string|null $user Usuario user.
     */
    public function isAuthorized($user) {
        return true;
    }
}
