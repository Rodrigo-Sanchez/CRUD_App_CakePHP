<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Network\Session;
use Cake\Validation\Validator;

/**
 * Usuarios Model
 *
 * @method \App\Model\Entity\Usuario get($primaryKey, $options = [])
 * @method \App\Model\Entity\Usuario newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Usuario[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Usuario|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Usuario|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Usuario patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Usuario[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Usuario findOrCreate($search, callable $callback = null, $options = [])
 */
class UsuariosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('usuarios');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->hasMany('Registros');

        // Add the behaviour and configure any options you want
        $this->addBehavior('Proffer.Proffer', [
            'photo' => [ // The name of your upload field
                'root' => WWW_ROOT . 'files', // Customise the root upload folder here, or omit to use the default
                'dir' => 'photo_dir', // The name of the field to store the folder
                'thumbnailSizes' => [ // Declare your thumbnails
                    'square' => [ // Define the prefix of your thumbnail
                        'w' => 300,	// Width
                        'h' => 300,	// Height
                        'crop' => true,
                        'jpeg_quality'	=> 100
                    ]
                ],
                'thumbnailMethod' => 'gd' // Options are Imagick or Gd
            ]
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->allowEmptyString('email', false)
            ->add('email', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('password')
            ->maxLength('password', 255)
            ->requirePresence('password', 'create')
            ->allowEmptyString('password', false, 'Llenar este campo');

        /**
         * TODO: AGREGAR VALIDACIONES PARA LOS CAMPOS DE CAMBIO DE CONTRASEÑA.
         */
        $validator
            ->requirePresence(['actualPassword', 'newPassword', 'confirmPassword']);

        $validator
            ->scalar('nombre')
            ->maxLength('nombre', 255)
            ->requirePresence('nombre', 'create')
            ->allowEmptyString('nombre', false)
            ->add('nombre', 'validFormat', [
                'rule' => ['custom', '/^[a-zA-ZñÑáéíóúÁÉÍÓÚ]+(?:[\s]+[a-zA-ZñÑáéíóúÁÉÍÓÚ]+)*$/'],
                'message' => 'Digita solamente caracteres alfabéticos sin espacios iniciales ni finales.']);

        $validator
            ->scalar('apellidopaterno')
            ->maxLength('apellidopaterno', 255)
            ->requirePresence('apellidopaterno', 'create')
            ->allowEmptyString('apellidopaterno', false)
            ->add('apellidopaterno', 'validFormat', [
                'rule' => ['custom', '/^[a-zA-ZñÑáéíóúÁÉÍÓÚ]+(?:[\s]+[a-zA-ZñÑáéíóúÁÉÍÓÚ]+)*$/'],
                'message' => 'Digita solamente caracteres alfabéticos sin espacios iniciales ni finales.']);

        $validator
            ->scalar('apellidomaterno')
            ->maxLength('apellidomaterno', 255)
            ->allowEmptyString('apellidomaterno')
            ->add('apellidomaterno', 'validFormat', [
                'rule' => ['custom', '/^[a-zA-ZñÑáéíóúÁÉÍÓÚ]+(?:[\s]+[a-zA-ZñÑáéíóúÁÉÍÓÚ]+)*$/'],
                'message' => 'Digita solamente caracteres alfabéticos sin espacios iniciales ni finales.']);

        $validator
            // Cargamos el proveedor del plugin.
            ->provider('proffer', 'Proffer\Model\Validation\ProfferRules')
            // Set the thumbnail resize dimensions
            ->add('photo', 'proffer', [
                'rule' => ['dimensions', [
                    'min' => ['w' => 100, 'h' => 100],
                    'max' => ['w' => 1000, 'h' => 1000]
                ]],
                'message' => 'La imagen no tiene dimensión entre 100x100/1000x1000 pixeles.',
                'provider' => 'proffer',
            ])
            ->add('photo', 'extension', [
                'rule' => ['extension', [
                    'jpg', 'gif', 'png'
                ]],
                'message' => 'La imagen no tiene extensión correcta, ya sea jpg, gif o png.',
            ])
            ->add('photo', 'fileSize', [
                'rule' => ['fileSize', '<=', '1MB'],
                'message' => 'La imagen no debe exceder de 1MB de peso.',
            ])
            ->add('photo', 'mimeType', [
                'rule' => ['mimeType', ['image/jpeg', 'image/png']],
                'message' => 'La imagen no tiene un formato correcto.',
            ])
            ->requirePresence('photo', 'create')
            ->notEmpty('photo', null, 'create');

        $validator
            ->boolean('estatus')
            ->allowEmptyString('estatus');

        $validator
            ->boolean('rol')
            ->allowEmptyString('rol');

        return $validator;
    }

    public function validationPasswords($validator)
    {
        // Validator para que las dos contraseñas nuevas sean iguales.
        $validator
            ->sameAs('newPassword', 'confirmPassword', 'Las contraseñas deben de coincidir.');

        // Validator para la contraseña segura.
        $validator
            ->add('newPassword','validFormat',[
                'rule' => ['custom', '/^([A-Z]+[a-z]+[\d]+[%&@#$^*¡!¿?_,.~]+)*$/'],
                'message' => 'Debe contener al menos una mayúscula, una minúscula, un número y un caracter especial.'])
            ->minLength('newPassword', 8, 'La contraseña mínima es de 8 caracteres.')
            ->maxLength('newPassword', 15, 'La contraseña máxima es de 15 caracteres.');

        return $validator;
    }
    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }
}
