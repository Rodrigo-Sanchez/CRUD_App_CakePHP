# CRUD App CakePHP

CRUD de usuarios en un sistema creado con CakePHP.

## Descripción

Este repositorio fue creado para la segunda asignación del programa de becarios correspondiente al semestre 2019-II con sede en la *Dirección General de Cómputo y de Tecnologías de Información y Comunicación* de la *Universidad Nacional Autónoma de México*.

## Autor

* Sánchez Morales Rodrigo Alejandro.

### Contacto

<rodrigosanchez@ciencias.unam.mx>

## Asesor

* Barajas Gonzalez Daniel.

### Contacto

<danielbg.dev@gmail.com>