CREATE TABLE usuarios (
    id SERIAL PRIMARY KEY,
    email VARCHAR(255) NOT NULL UNIQUE,
    password VARCHAR(255) NOT NULL,
    nombre VARCHAR(255) NOT NULL,
    apellidoPaterno VARCHAR(255) NOT NULL,
    apellidoMaterno VARCHAR(255),
    estatus BOOLEAN DEFAULT TRUE,
    rol BOOLEAN DEFAULT FALSE,
    photo VARCHAR(255),
    photo_dir VARCHAR(255)
);

CREATE TABLE registros (
    id SERIAL,
    usuarioId INTEGER NOT NULL,
    email VARCHAR(255) NOT NULL,
    accion INTEGER NOT NULL,
  	fechaCreacion TIMESTAMP NOT NULL,
  	PRIMARY KEY (id, usuarioId),
  	FOREIGN KEY (usuarioId) REFERENCES usuarios (id)
);

-- Las contraseñas por defecto de todos los usuarios es hola123.

-- Usuario administrador.
INSERT INTO usuarios (email, password, nombre, apellidoPaterno, apellidoMaterno, rol, photo, photo_dir)
VALUES
('rodrigosanchez@ciencias.unam.mx', '$2y$10$gMZGVxKcDvSSaCxxmH80..zUnSk.f.VhbW42EWCWTNVfPY5MYYQhG', 'Rodrigo', 'Sánchez', 'Morales', TRUE, '01.png', 'd0d8cdb7-b864-49b1-b33a-6768a34f5b7f');

-- Usuarios no administradores.
INSERT INTO usuarios (email, password, nombre, apellidoPaterno, apellidoMaterno, photo, photo_dir)
VALUES
('cristina@gmail.com', '$2y$10$gMZGVxKcDvSSaCxxmH80..zUnSk.f.VhbW42EWCWTNVfPY5MYYQhG', 'Cristina', 'Viera', 'Avila', '02.png', 'fb7da72d-3816-4941-8f1e-056a2690b3a8'),
('alan@gmail.com', '$2y$10$gMZGVxKcDvSSaCxxmH80..zUnSk.f.VhbW42EWCWTNVfPY5MYYQhG', 'Alan', 'Gutiérrez', 'Bañuelos', '03.png', '2f60eeb2-4b6b-4c5c-9684-0031976879eb'),
('ulises@gmail.com', '$2y$10$gMZGVxKcDvSSaCxxmH80..zUnSk.f.VhbW42EWCWTNVfPY5MYYQhG', 'Ulises', 'Herrera', 'Martínez', '04.png', 'ce6f9a17-3264-4b38-8ce4-a78a957df547'),
('andres@gmail.com', '$2y$10$gMZGVxKcDvSSaCxxmH80..zUnSk.f.VhbW42EWCWTNVfPY5MYYQhG', 'Andres', 'Martínez', 'González', '05.png', '9aa15d58-e61b-4279-9811-b05a59bebac9'),
('david@gmail.com', '$2y$10$gMZGVxKcDvSSaCxxmH80..zUnSk.f.VhbW42EWCWTNVfPY5MYYQhG', 'David', 'Lira', 'Espíndola', '06.png', 'e9ea06b5-0473-48a2-8d8f-1c81c9f611c0'),
('juan@gmail.com', '$2y$10$gMZGVxKcDvSSaCxxmH80..zUnSk.f.VhbW42EWCWTNVfPY5MYYQhG', 'Juan', 'Bravo', 'De los Monteros', '07.png', '0288c592-1f39-44ee-b06f-deedc2e8cab9'),
('victor@gmail.com', '$2y$10$gMZGVxKcDvSSaCxxmH80..zUnSk.f.VhbW42EWCWTNVfPY5MYYQhG', 'Victor Manuel', 'Ruíz', 'Albor', '08.png', '6a563bb7-d47e-4996-ab24-b4268d02e430'),
('natalia@gmail.com', '$2y$10$gMZGVxKcDvSSaCxxmH80..zUnSk.f.VhbW42EWCWTNVfPY5MYYQhG', 'Natalia', 'Muníz', 'Aguirre', '09.png', 'a7a414ad-375d-40f3-9a6b-c2729f76fdce'),
('daniel@gmail.com', '$2y$10$gMZGVxKcDvSSaCxxmH80..zUnSk.f.VhbW42EWCWTNVfPY5MYYQhG', 'Daniel', 'Sánchez', 'Maqueda', '10.png', '74337993-c6a3-4f0a-8697-c5c13bfd655e'),
('fausto@gmail.com', '$2y$10$gMZGVxKcDvSSaCxxmH80..zUnSk.f.VhbW42EWCWTNVfPY5MYYQhG', 'Fausto Felipe', 'Morales', 'Acevedo', '11.png', 'e5b7fe89-22ba-4ce0-8b0f-54d23c4fdfb7'),
('javier@gmail.com', '$2y$10$gMZGVxKcDvSSaCxxmH80..zUnSk.f.VhbW42EWCWTNVfPY5MYYQhG', 'Francisco Javier', 'González', 'Perivan', '12.png', '77a46578-da65-49d2-9c73-58e7a835c34b'),
('francisco@gmail.com', '$2y$10$gMZGVxKcDvSSaCxxmH80..zUnSk.f.VhbW42EWCWTNVfPY5MYYQhG', 'Francisco', 'Martínez', 'Santander', '13.png', 'd0db195a-b09f-460e-bf2c-2d87e4a660a4'),
('mariana@gmail.com', '$2y$10$gMZGVxKcDvSSaCxxmH80..zUnSk.f.VhbW42EWCWTNVfPY5MYYQhG', 'Mariana', 'Legaria', 'Tellez', '14.png', '64e97237-cbde-4235-822b-fd148e945516'),
('erika@gmail.com', '$2y$10$gMZGVxKcDvSSaCxxmH80..zUnSk.f.VhbW42EWCWTNVfPY5MYYQhG', 'Erika', 'Quiroz', 'Montaño', '15.png', '3c373ea1-37f5-4da3-9550-fcb385ef42ca'),
('veronica@gmail.com', '$2y$10$gMZGVxKcDvSSaCxxmH80..zUnSk.f.VhbW42EWCWTNVfPY5MYYQhG', 'Verónica', 'Lira', 'Termópolis', '16.png', '19087d47-ad24-4121-bf46-257dc8802193');